const webpack = require('webpack')
const WebpackShellPlugin = require('webpack-shell-plugin')

const NODE_ENV = process.env.NODE_ENV || 'development';

module.exports = {
	devtool: 'cheap-inline-module-source-map',
	//NODE_ENV == 'development' ? 'eval' : 'cheap-inline-module-eval-source-map',
	watch: true,
	watchOptions: {
		aggregateTimeout: 50
	},
	entry: ['babel-polyfill', './entry'],
	output: {
		filename: './bundle.js',
		library: 'entry'
	},
	module: {
		loaders: [
			{
				test: /\.js$/,
				exclude: /node_modules/,
				loader: "babel-loader",
				plugins: ['transform-runtime'],
        presets: ['es2015', 'stage-0'],
			}
		]
	},
	plugins: [
    new WebpackShellPlugin({onBuildStart:['echo "Webpack Start"'], onBuildEnd:['echo "Webpack End" && npm run jsW']})
  ]
	// module: [{
	//   loaders: [{
	// 		test: /\.js$/,
	// 		exclude: /node_modules/,
	// 		loader: "babel" }
	//   ]
	// }]
	// module: [{
	// 	loaders: [{
	// 		test: /\.ja$/,
	// 		exclude: /(node_modules)/,
	// 		loader: 'babel?optional[]=runtime&stage=0'
	// 	}]
	// }]
}
