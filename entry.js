// async function asyncFun () {
//   var value = await new Promise((resolve, reject) => {
// 		let x = 10;
// 		resolve(x);
// 	})
//     .then(x => x * 3)
//     .then(x => x + 5)
//     .then(x => x / 2);
//   return value;
// }
// asyncFun().then(x => console.log(`x: ${x}`));


// export default async function foo() {
//   var s = await bar();
//   console.log(s);
// }
//
// function bar() {
//   return "bar";
// }
//
// let test = foo();
// console.log(test);


function getRainbow() {
	return new Promise((resolve, reject) => {
		resolve('RAINBOW =)');
	});
}

function getButterfly() {
	return new Promise(function(resolve, reject) {
		setTimeout(()=>{
			resolve(' New BUTTERFly =) ');
		}, 2000)
	});
}

async function unicorn() {
	let rainbow = await getRainbow();
	let butterfly = await getButterfly();
	return rainbow + butterfly;
}

console.log(
	unicorn().then(res => {
		console.log(res);
	})
);



// const p1 = fetch('http://www.omdbapi.com/?t=The Matrix');
// const p2 = fetch('http://www.omdbapi.com/?t=Forrest Gump');
// const [r1, r2] = await Promise.all([p1, p2]);
// const [movie1, movie2] = await Promise.all([r1.json(), r2.json()]);

// exports.hello = function hello() {
// 	console.log('FUNCTION HELLO 2');
// }
//
// import second from './second'
//
// second();
